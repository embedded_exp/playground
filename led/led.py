import machine
import utime as time
import math


class Breathe(object):

    def __init__(self, pin=2):
        self.led = machine.PWM(machine.Pin(pin), freq=1000)

    def run(self, delay):
         for i in range(200):
            self.led.duty(int(math.sin(i / 100 * math.pi) * 500 + 500))
            time.sleep_ms(delay)
