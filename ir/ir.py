class IrProto(object):
    SONY = "SONY"
    NEC = "NEC"


class IrSend(object):
    """This is just an example of custom MP module.
       IR module for micropython"""

    def __init__(self, pin=2, proto=IrProto.SONY):
        print("This is IR send class implementation")
        print("Using device PIN: {}".format(pin))
        print("Using proto: {}".format(proto))

        self._pin = pin
        self._proto = proto

