from led.led import Breathe
from ir.ir import IrSend, IrProto
from dht12 import dht12
from machine import I2C, Pin

sender = IrSend(pin=2, proto=IrProto.SONY)
breathe_led = Breathe(pin=2)

i2c = I2C(scl=Pin(5), sda=Pin(4))
sensor = dht12.DHT12(i2c)

while True:
    breathe_led.run(10)

    temp, hum = sensor.measure()
    print(temp)
    print(hum)
